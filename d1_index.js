// Advance Query Operators
    // We want more flexible querying of data within MongoDB

//[SECTION] Comparison Query Operators

    //$gt - greater than
    //gte - greater than equal
    /*
    syntax:
        db.collectionName.find({field: {$gt/$gte: value}});
    */

db.users.find(
    {
        age:{$gt:5}
    }
);

db.users.find(
    {
        age:{$gte:5}
    }
);


//$lt -less than
//$lte - less than or equal

db.users.find(
    {
        age: {$lt:82}
    }
);

db.users.find(
    {
        age: {$lte:82}
    }
);

// $ne - not equal

db.users.find(
    {
        age:{$ne:82}
    }
);

//$in 
/*
    -Allows us to find documents with specific match criteria with one field using different values
    syntax:
        db.collectionName.find(field:{$in: [valueA, valueB]})
*/

db.users.find(
    {
        lastName:{
            $in:['Hawking', 'Doe']
        }
    }
);

db.users.find(
    {
        courses:{
            $in: ['HTML', 'React']
        }
    }
);

// $or (1 true = true)
/*
    db.collectionName.find({$or:[{fieldA:valueA}, {fieldB:valueB}]})
*/
db.users.find(
    {
        $or: [
            {firstName: "Niel"},
            {age: 25}
        ]	
    }
);

//$and (1 false = false)
/*
     db.collectionName.find({$and:[{fieldA:valueA}, {fieldB:valueB}]})
*/

db.users.find(
    {
        $and: [
            {age: {$ne:82}},
            {age: {$ne:76}}
        ]
    }
);

//[SECTION] Field Protection
//help with readability of the values returned, we can include/exclude field from  the response
/*
    syntax:
    db.collectionName.find({criteria},{fields you want to include, it should have a value of 1});
*/

//inclusion (1)
db.users.find(
    {   
        //criteria
        firstName: "Jane"
    },
    {
        firstName:1,
        lastName:1,
        contact:1
    }
);

// Exclusion (0)
db.users.find(
    {   
        //criteria
        firstName: "Jane"
    },
    {
        department:0,
        contact:0
    }
);

//Supressing the ID field
db.users.find(
    {
        //criteria
        firstName: "Jane"
    },
    {
        firstName:1,
        lastName:1,
        contact:1,
        department:0
    }
);

// $regex
/*

*/
//case sensitive
db.users.find(
    {
        firstName:{
            $regex: 'N'
        }
    }
);

//not case sensitive
db.users.find(
    {
        firstName:{
            $regex: 'N', $options:'$i'
        }
    }
);
